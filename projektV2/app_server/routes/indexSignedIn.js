var express = require('express');
var router = express.Router();
var ctrlIndexSignedIn = require('../controllers/indexSignedIn');
/* GET home page. */
router.get('/', ctrlIndexSignedIn.indexSignedIn);
router.post('/poslji', ctrlIndexSignedIn.pridobiAvte);
module.exports = router;
