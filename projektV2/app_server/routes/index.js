var express = require('express');
var router = express.Router();
var ctrlIndex = require('../controllers/index');
var ctrlAvtomobili = require('../controllers/avtomobili');
var ctrlDodajAvto = require('../controllers/dodajAvto');
var ctrlDodajTocenjeGoriva = require('../controllers/dodajTocenjeGoriva');
var ctrlEditProfile = require('../controllers/editProfile');
var ctrlFaq = require('../controllers/faq');
var ctrlKalulator = require('../controllers/kalkulator');
var ctrlKontakt = require('../controllers/kontakt');
var ctrlSignIn = require('../controllers/signin');
var ctrlSignUp = require('../controllers/signup');
var ctrlIndexSignedIn = require('../controllers/indexSignedIn');
var ctrlAdmin = require('../controllers/admin');


/* GET home page. */
router.get('/', ctrlIndex.index);

/* Ostale strani. */
router.get('/avtomobili', ctrlAvtomobili.avtomobili);
router.get('/dodajAvto', ctrlDodajAvto.dodajAvto);
router.get('/dodajTocenje', ctrlDodajTocenjeGoriva.dodajTocenjeGoriva);
router.get('/urediProfil', ctrlEditProfile.editProfile);
router.get('/faq', ctrlFaq.faq);
router.get('/kalkulator', ctrlKalulator.kalkulator);
router.get('/kontakt', ctrlKontakt.kontakt);
router.get('/signin', ctrlSignIn.signIn);
router.get('/signup', ctrlSignUp.signUp);
router.get('/index-signedin', ctrlIndexSignedIn.indexSignedIn);
router.get('/dodajAvtoAdmin', ctrlAdmin.dodajAvtoAdmin);
router.get('/odstraniUporabnike', ctrlAdmin.odstraniUporabnike);
router.post('/dodajAvtoAdmin', ctrlAdmin.shraniAvto);
router.post('/signup', ctrlSignUp.dodajUporabnika);
module.exports = router;
