var express = require('express');
var router = express.Router();
var ctrldb = require('../controllers/db');
/* GET home page. */
router.get('/', ctrldb.db);
router.post('/izbrisVseh', ctrldb.izbrisiVse);
router.post('/dodajanjePodatkov', ctrldb.dodajPodatke);

module.exports = router;