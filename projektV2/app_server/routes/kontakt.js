var express = require('express');
var router = express.Router();
var ctrlKontakt = require('../controllers/kontakt');
/* GET home page. */
router.get('/', ctrlKontakt.kontakt);

module.exports = router;
