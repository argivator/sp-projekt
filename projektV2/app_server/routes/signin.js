var express = require('express');
var router = express.Router();
var ctrlSignIn = require('../controllers/signin');
/* GET home page. */
router.get('/', ctrlSignIn.signIn);
router.post('/', ctrlSignIn.checkLogin);

module.exports = router;
