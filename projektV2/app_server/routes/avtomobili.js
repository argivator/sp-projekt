var express = require('express');
var router = express.Router();
var ctrlAvtomobili = require('../controllers/avtomobili');
/* GET home page. */

router.get('/avtomobili', ctrlAvtomobili.avtomobili);
router.post('/pridobi', ctrlAvtomobili.navediModele);

module.exports = router;