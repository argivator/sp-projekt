var express = require('express');
var router = express.Router();
var ctrlFaq = require('../controllers/faq');
/* GET home page. */
router.get('/', ctrlFaq.faq);

module.exports = router;