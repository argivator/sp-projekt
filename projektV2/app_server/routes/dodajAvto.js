var express = require('express');
var router = express.Router();
var ctrlDodajAvto = require('../controllers/dodajAvto');
/* GET home page. */
router.get('/', ctrlDodajAvto.dodajAvto);
router.post('/', ctrlDodajAvto.uporabnikDodal);

module.exports = router;
