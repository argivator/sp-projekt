var express = require('express');
var router = express.Router();
var ctrlEditProfile = require('../controllers/editProfile');
/* GET home page. */
router.get('/', ctrlEditProfile.editProfile);

module.exports = router;
