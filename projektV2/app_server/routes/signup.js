var express = require('express');
var router = express.Router();
var ctrlSignUp = require('../controllers/signup');
/* GET home page. */
router.get('/', ctrlSignUp.signUp);

module.exports = router;
