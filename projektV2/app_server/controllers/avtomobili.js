var request = require('request');
var apiURL = {
  server: "http://localhost:" + process.env.PORT
};

if (process.env.API_URL) {
  apiURL.server = process.env.API_URL;
}

var znamkeTable = [];
var tabeleModelov = [];

module.exports.avtomobili = function(req, res, next){
    getAvtomobileAndReturnView(res);
    //res.render('avtomobili');
}

var getAvtomobileAndReturnView = function(res){
    request({
        url: apiURL.server + '/api/avtomobiliList',
        method: 'GET',
        json: {},
    }, function callback(error, code, body){
        //console.log(body);
        znamkeTable = body.sort();
        res.render('avtomobili', {znamke: znamkeTable, modeli: []})
    })
}

module.exports.navediModele = function(req, res, next){
    
    var data = req.body;
    var znamkaString = data.znamka;
    znamkaString = znamkaString.split("'");
    var znamka = znamkaString[1];
    
    request({
        url: apiURL.server + '/api/modeliIzZnamke/' + znamka,
        method: 'GET',
        json: {},
    }, function callback(error, code, body){
        res.render('avtomobili', {znamke: znamkeTable, modeli: body.sort()})
    })
}