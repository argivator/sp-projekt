var request = require('request');
var apiParametri = {
    streznik: "http://localhost:" + process.env.PORT
};
if (process.env.API_URL) {
  apiParametri.server = process.env.API_URL;
}

module.exports.signUp = function(req, res, next) {
    res.render('signup');
};

module.exports.dodajUporabnika = function(req, res) {
    var path = "/api/uporabnik";

    var data = {
      uporabniskoIme: req.body.username,
      email: req.body.email,
      password: req.body.password[0]
  };
    var params = {
            url: apiParametri.streznik + path,
            method: 'POST',
            json: data
    };
    
    request(
      params,
      function(napaka, odgovor, vsebina) {
        if (odgovor.statusCode == 201) {
          res.redirect('/index-signedin');
       } else {
          // Prikazi napako
        }
      }
    ); 
};