var request = require('request');
var apiURL = {
  server: "http://localhost:" + process.env.PORT
};

if (process.env.API_URL) {
  apiURL.server = process.env.API_URL;
}

module.exports.db = function(req, res, next){
    res.render('db');
}
module.exports.izbrisiVse = function(req, res, next){
    request({
        url: apiURL.server + '/api/db/izbrisiVse',
        method: 'POST',
        json: {},
    }, function callback(error, code, body){
        res.render('db');
    })
}
module.exports.dodajPodatke = function(req, res, next){
    console.log("dodajanje!");
    request({
        url: apiURL.server + '/api/db/dodajPodatke',
        method: 'POST',
        json: {},
    }, function callback(error, code, body){
       res.render('db'); 
    })
}