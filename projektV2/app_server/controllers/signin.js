
var request = require('request');
var apiURL  = {
    server: "http://localhost:" + process.env.PORT
}

if (process.env.API_URL) {
    apiURL.server = process.env.API_URL;
}


module.exports.signIn = function(req, res, next) {
   res.render('signin');
}


module.exports.checkLogin = function(req, res, next) {
  var data = req.body;
  var username = data.username;
  var password = data.password;
  
  
  var apiParams = {
      url: apiURL.server + "/api/uporabnik/" + username + "/" + password,
      method: 'GET',
      json: {}
  }
  request(
      apiParams,
      function(err,response, content){
          if(content.obstaja){
              res.redirect('/home');
          }else{
              res.render('signin', {error: true, username: username, password: password});
          }
      }
  )
}

