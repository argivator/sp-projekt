var request = require('request');
var apiParametri = {
    streznik: "http://localhost:" + process.env.PORT
};
if (process.env.API_URL) {
  apiParametri.server = process.env.API_URL;
}

module.exports.dodajAvtoAdmin = function(req, res, next) {
    res.render('dodajAvtoAdmin');
};

module.exports.odstraniUporabnike = function(req, res, next) {
    res.render('odstraniUporabnike');
};


module.exports.shraniAvto = function(req, res) {
  var path = "/api/avtomobil";
  var data = {
      znamka: req.body.znamka,
      model: req.body.model
  };
  var params = {
    url: apiParametri.streznik + path,
    method: 'POST',
    json: data
  };
  request(
      params,
      function(napaka, odgovor, vsebina) {
        if (odgovor.statusCode == 201) {
          res.redirect('/dodajAvtoAdmin');
        } else if (odgovor.statusCode === 400 && vsebina.name && vsebina.name === "ValidationError") {
          res.redirect('/dodajAvtoAdmin');
        } else {
          // Prikazi napako
        }
      }
    );    
};