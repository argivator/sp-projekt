var request = require('request');
var apiParametri = {
    streznik: "http://localhost:" + process.env.PORT
};
if (process.env.API_URL) {
  apiParametri.server = process.env.API_URL;
}


module.exports.dodajAvto = function(req, res, next) {
   res.render('dodajAvto');
}

module.exports.uporabnikDodal = function(req, res, next){
  var path = "/api/dodajAvtoUpor";
   
  var data = {
      znamka: req.body.znamka,
      model: req.body.model,
      letnik: req.body.letnik,
      lastnik: req.body.lastnik
  };
  var params = {
    url: apiParametri.streznik + path,
    method: 'POST',
    json: data
  };
  request(
      params,
      function(napaka, odgovor, vsebina) {
        if (odgovor.statusCode == 201) {
          res.redirect('/dodajAvto');
        } else if (odgovor.statusCode === 400 && vsebina.name && vsebina.name === "ValidationError") {
          res.redirect('/dodajAvto');
        } else {
          // Prikazi napako
        }
      }
    );    
}