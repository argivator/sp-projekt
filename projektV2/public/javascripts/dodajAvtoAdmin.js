$(document).ready(function() {
   var errMsg = "Izpolnite vsa polja."
   
   $("#loginbutton").click(function(event) {
       $("#errorMessage").hide();
       var form = $("#signinform");
       // uporabljeni id-ji od login-forma (ker so v enakem slogu)
       var znamka = $("#login-username").val();
	   var model = $("#login-password").val();
	   
	   if (znamka.length < 1 || model.length < 1) {
	       $("#errorMessage").html(errMsg);
		   $("#errorMessage").show();
	   }else {
	       $("#errorMessage").show().html("SUCCESS!").css('color', 'green');
	       form.submit();
	   }
	   
	   
	   event.preventDefault();
   });
});