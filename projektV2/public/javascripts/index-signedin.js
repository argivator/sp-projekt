$(document).ready(function() {


    
    //  SELEKTOR AVTOMOBILA
	$("#myCarSelect").click(function(event) {
	    $("#povprecnaPoraba").empty();
	    $("#dodajTocenjeGoriva").hide();
		$("#grafPorabe").empty();
		$("#komentarji").empty();
		$("#dodajKomentarButton").hide();
		var n = $("#myCarSelect").val();
		
		if (n != "") {
		    izpisiKomentarje();
		    $("#dodajTocenjeGoriva").show();
		}

		for (var i in myCars) {
		    if (myCars[i].id == n) {
		        
		        
		        var datumi = [];
		        var porabe = [];
		        for (var j in myCars[i].tankanja) {
		            if (j==0) continue;
		            datumi.push(myCars[i].dates[j]);
		            
		            porabe.push(getConsumption(myCars[i].tankanja[j], myCars[i].stanjeStevca[j-1], myCars[i].stanjeStevca[j]));
		        }
		        
		        $("#povprecnaPoraba").append(`<h3><b>Povprečna poraba:</b> ${getAvrege(porabe).toFixed(2)}</h3>`);
		        
		        // GRAPH
		        var trace1 = {
                  x: datumi,
                  y: porabe,
                  type: 'scatter'
                }; 
                var data = [trace1];
		        Plotly.newPlot('grafPorabe', data);
		        
		    }
		}
		
	});
	
	
	
	//  BUTTON "DODAJ AVTOMOBIL"
	$("#dodajAvtomobil").click(function(event) {
	   window.location = "/dodajAvto";
	});
	
	//BUTTON "DODAJ TOCENJE GORIVA"
	$("#dodajTocenjeGoriva").click(function(event) {
	   window.location = "/dodajTocenje"; 
	});
	
	
	
	
	//  BUTTON "DODAJ KOMENTAR"
	$("#dodajKomentarButton").click(function(event) {
	   $("#dodajKomentarButton").hide();
	   $("#addComment").append(`
	   <form id="addCommentForm" role="form" method="post">
	        <input id="author" type="text" name="author" value="" placeholder="Avtor" required>
	        <br>
	        <textarea name="comment" id="comment"></textarea>
	        <br>
	   </form>`);
	   document.getElementById("addCommentForm").appendChild(komentirajButton);
	   event.preventDefault();
	});
    
    
    
    
    
    // BUTTON "KOMENTIRAJ!"
    var komentirajButton = document.createElement('button');
    komentirajButton.innerHTML = "Komentiraj!";
    
    komentirajButton.onclick = function(event) {
        if ($("#author").val().length < 1 || $("#comment").val().length < 1) {
            $("#errorMessage").show();
        } else {
            $("#errorMessage").hide();
            $("#dodajKomentarButton").show();
            var comment = {avtor: $("#author").val(), komentar: $("#comment").val()};
            komentarji.push(comment);
            $("#addComment").empty();
            $("#komentarji").empty();
            izpisiKomentarje();
        }
        event.preventDefault();
    };
});

var myCars = [
    {id: 1, znamka: "Honda", model: "Civic", tankanja: [45, 42, 48, 46] /* v Litrih */, stanjeStevca: [250367, 250717, 251117, 251517] /* v km */, 
        dates: ['15-9-2017', '3-10-2017', '28-10-2017', '25-11-2017']}
    ];
    
    

//  izracuna porabo glede na kilometre od prejsnjega tankanja in litre trenutnega tankanja (predvideva se obakrat poln tank)
function getConsumption(tankanje2, km1, km2) {
    var km = km2-km1;
    return tankanje2/km*100;
}

// izracuna povprecje tabele stevil (povprecno porabo vecih porab)
function getAvrege(x) {
    var sum = 0;
    for (i in x) {
        sum += x[i];
    }
    return sum/x.length;
}


var komentarji = [];



//  izpise vse komentarje
function izpisiKomentarje() {
    $("#komentarji").append(`<h3>Komentarji: </h3><div id="noComments"></div>`);
    if (komentarji.length < 1) $("#noComments").append("(Trenutno še ni komentarjev)");
    else {
        for (i in komentarji) {
            $("#komentarji").append(`<div class="komentar"><b>${komentarji[i].avtor}</b> <br> ${komentarji[i].komentar}</div>`);
        }
    }
    $("#dodajKomentarButton").show();
}