$(document).ready(function() {
    var znamka;
    $("#make").click(function(event) {
        znamka = $("#make").val();
	    
	    
	    if (znamka == "Audi") {
	        
	        //enable model selector
    	    $("#model").prop('disabled', false);
    	    
	        //add models to html
	        for (var i in audi) {
	            $("#model").append(`<option value="${audi[i].model}">${audi[i].model}</option>`);
	        }
	        
	    } else if (znamka == "Honda") {
	         //enable model selector
    	    $("#model").prop('disabled', false);
    	    
	        //add models to html
	        for (var i in honda) {
	            $("#model").append(`<option value="${honda[i].model}">${honda[i].model}</option>`);
	        }
	    }else {
	        
	        //disable model selector
	        $("#model").val("");
            $("#model").prop('disabled', true);
            
            //disable year selector
            $("#year").val("");
            $("#year").prop('disabled', true);
	    }
    });
    
    

    $("#model").click(function(event) {
        var model = $("#model").val();
        
        if (model.length > 0) {
            
            //enable year selector
            $("#year").prop('disabled', false);
            
        } else {

            //disable year selector
            $("#year").val("");
            $("#year").prop('disabled', true);
        }
    })
    
    
    $("#dodajButton").click(function(event) {
        $("#errorMessage").hide();
        $("#succesMessage").hide();
        
        var znamka = $("#make").val();
        var model = $("#model").val();
        var letnik = $("#year").val();
        

        if (letnik.length > 0) {    // moramo izbrati vse 3 lastnosti
            $("#succesMessage").show();
            resetFields();
        } else {
            $("#errorMessage").show();
        }
        event.preventDefault();
    });

	
	
	
});

var audi = [
	    {model: "50"},
	    {model: "60"},
	    {model: "80"},
	    {model: "90"},
	    {model: "100"},
	    {model: "200"},
        {model: "A1"},
        {model: "A2"},
        {model: "A3"},
        {model: "A4"},
        {model: "A5"},
        {model: "A6"},
        {model: "A7"},
        {model: "A8"},
        {model: "Allroad"},
        {model: "Coupe"},
        {model: "Cabriolet"},
        {model: "Q1"},
        {model: "Q2"},
        {model: "Q3"},
        {model: "Q5"},
        {model: "Q7"},
        {model: "R8"},
        {model: "RS2"},
        {model: "RS3"},
        {model: "RS4"},
        {model: "RS5"},
        {model: "RS6"},
        {model: "RS7"},
        {model: "RS Q3"},
        {model: "S1"},
        {model: "S2"},
        {model: "S3"},
        {model: "S4"},
        {model: "S5"},
        {model: "S6"},
        {model: "S7"},
        {model: "S8"},
        {model: "SQ5"},
        {model: "SQ7"},
        {model: "TT"},
        {model: "V8"}
	    ];
	    
var honda = [
    {model: "Civic"},
    {model: "Accord"}
    ];
	

function resetFields() {
    $("#make").val("");
    $("#model").val("");
    $("#year").val("");
    $("#model").prop('disabled', true);
    $("#year").prop('disabled', true);
}