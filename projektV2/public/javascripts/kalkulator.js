$(document).ready(function() {
    
    for (var i in myCars) {
        $("#myCarSelect").append(`<option value="${myCars[i].id}">${myCars[i].znamka} ${myCars[i].model}</option>`);
    }
    
    $("#izracunaj").click(function(event) {
        $("#errorMessage").hide();
        $("#result").empty();
        var carId = $("#myCarSelect").val();
        var km = $("#kilometri").val();
        var gorivo = $("#petrolType").val();
        var avgPoraba = 11.80;
        var result;
        
        if (carId=="" || km < 0.1) {
            $("#errorMessage").show();
        }
        else {
            switch (gorivo) {
                case '1':
                    result = cenaBencin * avgPoraba * km/100;
                    break;
                
                case '2':
                    result = cenaDizla * avgPoraba * km/100;
                    break;
                    
                case '3':
                    result = cenaLPG * avgPoraba * km/100;
                    break;
            }
            
            $("#result").append(`<b>Pričakovani stroški:</b> ${result.toFixed(2)}€`);
        }
        event.preventDefault();
    });
    
});

var myCars = [
    {id: 1, znamka: "Honda", model: "Civic", tankanja: [45, 42, 48, 46] /* v Litrih */, stanjeStevca: [250367, 250717, 251117, 251517] /* v km */, 
        dates: ['15-9-2017', '3-10-2017', '28-10-2017', '25-11-2017']}
    ];
    
var cenaBencin = 1.352;
var cenaDizla = 1.233;
var cenaLPG =  0.674;