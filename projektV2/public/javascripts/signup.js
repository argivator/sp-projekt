$(document).ready(function() {
	var errMsg1 = "Izpolnite vsa polja."
	var errMsg2 = "Vnešeno uporabniško ime že obstaja."
	var errMsg3 = "Gesli se ne ujemata."
    
	$("#loginbutton").click(function(event) {
		$("#errorMessage").hide();
		var username = $("#signup-username").val();
		var password = $("#signup-password").val();
		var passwordConfirm = $("#signup-password-confirm").val();
		
		if (username.length < 1 ||  $("#signup-email").val().length < 1 || password.length < 1 || passwordConfirm.length < 1) {
			$("#errorMessage").html(errMsg1);
			$("#errorMessage").show();
		} else if (password != passwordConfirm) {
			$("#errorMessage").html(errMsg3);
			$("#errorMessage").show();
		} else if (username == "jozek.honda") {
			$("#errorMessage").html(errMsg2);
		    $("#errorMessage").show();
		} else {
			$("#signinform").submit();
		}
		event.preventDefault();
	});

});