var mongoose = require('mongoose');

var avtomobili = new mongoose.Schema({
    znamka: {type: String, required: true},
    model: {type: String, required: true},
    letnik: Number,
    lastnik: String
})

mongoose.model('Avtomobil', avtomobili, 'Avtomobili');