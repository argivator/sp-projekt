var mongoose = require('mongoose');

var dbURI = 'mongodb://localhost/poraba';
if (process.env.NODE_ENV === 'production') {
    dbURI = process.env.MLAB_URI;
}
mongoose.connect(dbURI, { useMongoClient: true })



mongoose.connection.on('connected', function(){
    console.log('Databaza je povezana na ' + dbURI); 
})
mongoose.connection.on('error', function(err){
    console.log('Databaza napaka: ' + err); 
})
mongoose.connection.on('disconnected', function(){
    console.log('Databaza je disconnectana'); 
})

var pravilnaUstavitev = function(sporocilo, povratniKlic){
    mongoose.connection.close(function(){
        console.log('Databaza je zaprla povezavo preko  ' + sporocilo);
        povratniKlic();
    })
}
// izhod iz app
process.once('SIGUSR2', function(){
    pravilnaUstavitev('izhod iz aplikacije', function(){
        process.exit(0);
    })
})
// izhod iz heroku
process.on('SIGTERM', function(){
    pravilnaUstavitev('izhod iz aplikacije na heroku', function(){
        process.exit(0);
    })
})

require('./uporabniki');
require('./avtomobili');
