var mongoose = require('mongoose');

var tocenjaShema = new mongoose.Schema({
    datum: Date,
    kolicina: Number,
    stanjeStevca: Number,
    opombe: String
})

var avtomobiliShema = new mongoose.Schema({
    znamka: String,
    model: String,
    letnik: Number,
    ostalo: String,
    tocenja: [tocenjaShema]
})

var uporabnikiShema = new mongoose.Schema({
    uporabniskoIme: {type: String, required: true},
    email: {type: String, required: true},
    password: {type: String, required: true},
    avtomobili: [avtomobiliShema]
})

mongoose.model('Uporabnik', uporabnikiShema, 'Uporabniki');