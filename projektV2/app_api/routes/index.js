var express = require('express');
var router = express.Router();
var ctrlUporabniki = require('../controllers/uporabniki');
var ctrlAvtomobili = require('../controllers/avtomobili');
var ctrldb = require('../controllers/db');

//uporabniki
router.get('/uporabnik/:username/:password', ctrlUporabniki.uporabnikObstaja);
router.get('/uporabnik/:username', ctrlUporabniki.getIdByUsername);
router.post('/uporabnik', ctrlUporabniki.uporabnikKreiraj);
router.delete('/uporabnik/:idUporabnika', ctrlUporabniki.uporabnikIzbrisi);
router.put('/uporabnik/:idUporabnika', ctrlUporabniki.uporabnikPosodobi);
router.get('/uporabnikiList', ctrlUporabniki.listUporabnike);

//avtomobili

router.post('/avtomobil', ctrlAvtomobili.dodajAvtomobil);
router.get('/avtomobiliList', ctrlAvtomobili.listAvtomobili);
router.get('/modeliIzZnamke/:znamka', ctrlAvtomobili.navediModele);
router.post('/dodajAvtoUpor', ctrlAvtomobili.dodajAvtomobilUporabnik);
router.post('/avtomobiliUporabnika', ctrlAvtomobili.uporabnikoviAvtomobili);

// db

router.post('/db/izbrisiVse', ctrldb.izbrisiVse);
router.post('/db/dodajPodatke', ctrldb.dodajPodatke);

module.exports = router;