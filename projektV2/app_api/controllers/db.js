var mongoose = require('mongoose');

var Avtomobil = mongoose.model('Avtomobil');
var Uporabnik = mongoose.model('Uporabnik');

var returnJson = function(res, status, content){
    res.status(status);
    res.json(content);
};


module.exports.izbrisiVse = function(req, res){
    console.log("Ok deleting");
    Avtomobil.collection.remove();
    Uporabnik.collection.remove();
    returnJson();
}

module.exports.dodajPodatke = function(req, res){
    console.log("Dodajanje v kontrolerju db.js");
    var uporabnik1 = ['Mihec123', 'mihec@gmail.com', '123mihec'];
    var uporabnik2 = ['testing', 'testing@gmail.com', 'testing'];
    var uporabnik3 = ['Asistent', 'asistent@gmail.com', 'asistent'];
    
    var napaka = false;
    
    // dodajanje userjev
    
    Uporabnik.create({
        uporabniskoIme: uporabnik1[0],
        email: uporabnik1[1],
        password: uporabnik1[2]
    }, function(err, uporabnik) {
        if(err) napaka = true;
    });
    
    Uporabnik.create({
        uporabniskoIme: uporabnik2[0],
        email: uporabnik2[1],
        password: uporabnik2[2]
    }, function(err, uporabnik) {
        if(err) napaka = true;
    });
    
    Uporabnik.create({
        uporabniskoIme: uporabnik3[0],
        email: uporabnik3[1],
        password: uporabnik3[2]
    }, function(err, uporabnik) {
        if(err) napaka = true;
    });
    
    var avtomobil1 = ["BMW", "M3"];
    var avtomobil2 = ["BMW", "Z4"];
    var avtomobil3 = ["BMW", "530d"];
    var avtomobil4 = ["Honda", "Civic"];
    var avtomobil5 = ["Honda", "Prelude"];
    var avtomobil6 = ["Lancia", "Delta"];
    var avtomobil7 = ["Audi", "A5"];
    var avtomobil8 = ["Audi", "A3"];
    
    Avtomobil.create({
       znamka: avtomobil1[0],
       model: avtomobil1[1],
       letnik: '1999',
       lastnik: 'Mihec123'
    }, function(error, avto) {
        if (error) napaka = true;
    });
    Avtomobil.create({
       znamka: avtomobil2[0],
       model: avtomobil2[1],
       letnik: '2003',
       lastnik: 'Mihec123'
    }, function(error, avto) {
        if (error) napaka = true;
    });
    Avtomobil.create({
       znamka: avtomobil3[0],
       model: avtomobil3[1],
       letnik: '2010',
       lastnik: 'Asistent'
    }, function(error, avto) {
        if (error) napaka = true;
    });
    Avtomobil.create({
       znamka: avtomobil4[0],
       model: avtomobil4[1],
       letnik: '1999',
       lastnik: 'testing'
    }, function(error, avto) {
        if (error) napaka = true;
    });
    Avtomobil.create({
       znamka: avtomobil5[0],
       model: avtomobil5[1],
       letnik: '2003',
       lastnik: 'testing'
    }, function(error, avto) {
        if (error) napaka = true;
    });
    Avtomobil.create({
       znamka: avtomobil6[0],
       model: avtomobil6[1],
       letnik: '1988',
       lastnik: 'Asistent'
    }, function(error, avto) {
        if (error) napaka = true;
    });
    Avtomobil.create({
       znamka: avtomobil7[0],
       model: avtomobil7[1],
       letnik: '2003',
       lastnik: 'Asistent'
    }, function(error, avto) {
        if (error) napaka = true;
    });
    Avtomobil.create({
       znamka: avtomobil8[0],
       model: avtomobil8[1],
       letnik: '1999',
       lastnik: 'Mihec123'
    }, function(error, avto) {
        if (error) napaka = true;
    });
    
    
    
    
    
    
    
    
    if(!napaka) returnJson();
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
