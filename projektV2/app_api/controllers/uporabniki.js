var mongoose = require('mongoose');
var Uporabnik = mongoose.model('Uporabnik');

var returnJson = function(res, status, content){
    res.status(status);
    res.json(content);
}

module.exports.listUporabnike = function(req, res){
    Uporabnik.find().distinct('uporabniskoIme', function(err, result){
        returnJson(res, 200, result);
    })
}
module.exports.uporabnikObstaja = function(req, res){
    Uporabnik.findOne({"uporabniskoIme": req.params.username, "password": req.params.password})
    .exec(function(err, Uporabnik){
        var obstaja = (Uporabnik ? true : false);
        returnJson(res, 200, {"obstaja": obstaja, "uporabnik": req.params.username})
    })
}


module.exports.getIdByUsername = function(req, res){
   Uporabnik.findOne({"uporabniskoIme": req.params.username})
    .exec(function(napaka, uporabnik){
        if (napaka)
            returnJson(res, 400, napaka);
        else
            returnJson(res, 200, uporabnik.id)
   })
}

module.exports.uporabnikKreiraj = function(req, res){
    Uporabnik.create({
        uporabniskoIme: req.body.uporabniskoIme,
        email: req.body.email,
        password: req.body.password
    }, function(napaka, uporabnik) {
        console.log(uporabnik);
        if (napaka)
          returnJson(res, 400, napaka);
        else
          returnJson(res, 201, uporabnik);
    });
};

module.exports.uporabnikIzbrisi = function(req, res){
    var id = req.params.idUporabnika;
    if (id) {
        Uporabnik
            .findByIdAndRemove(id)
            .exec(
                function(napaka, uporabnik) {
                    if (napaka) {
                        returnJson(res, 404, napaka);
                        return;
                    }
                    returnJson(res, 204, null);
                })
    } else {
        returnJson(res, 404, {"sporočilo": "Ne najdem uporabnika, uporabniskoIme je obvezen parameter."});
    }
};

module.exports.uporabnikPosodobi = function(req, res) {
    if (!req.params.idUporabnika) {
   returnJson(res, 404, {"sporočilo": "Ne najdem uporabnika, idUporabnika je obvezen parameter."});
    return;
  }
  Uporabnik
    .findById(req.params.idUporabnika)
    .exec(
      function(napaka, uporabnik) {
        if (!uporabnik) {
          returnJson(res, 404, {"sporočilo": "Ne najdem uporabnika."});
          return;
        } else if (napaka) {
          returnJson(res, 400, napaka);
          return;
        }
        uporabnik.uporabniskoIme = req.body.uporabniskoIme;
        uporabnik.email = req.body.email;
        uporabnik.password = req.body.password;
        
        uporabnik.save(function(napaka, uporabnik) {
         if (napaka)
            returnJson(res, 404, napaka);
          else
            returnJson(res, 200, uporabnik);
        });
      }
    );
};