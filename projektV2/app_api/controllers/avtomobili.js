var mongoose = require('mongoose');

var Avtomobil = mongoose.model('Avtomobil');

var returnJson = function(res, status, content){
    res.status(status);
    res.json(content);
};
module.exports.dodajAvtomobil = function(req, res){
    Avtomobil.create({
       znamka: req.body.znamka,
       model: req.body.model
    }, function(napaka, avto) {
        if (napaka)
          returnJson(res, 400, napaka);
        else
          returnJson(res, 201, avto);
    });
};
module.exports.dodajAvtomobilUporabnik = function(req, res){
    //console.log(req.body);
    
    Avtomobil.create({
       znamka: req.body.znamka,
       model: req.body.model,
       lastnik: req.body.lastnik,
       letnik: req.body.letnik
    }, function(napaka, avto) {
        if (napaka)
          returnJson(res, 400, napaka);
        else
          returnJson(res, 201, avto);
    });
};

module.exports.listAvtomobili = function(req, res){
    /*Avtomobil.find().select('znamka').exec(function(err, result){
        returnJson(res, 200, result);
    })*/
    Avtomobil.find().distinct('znamka', function(err, result){
        returnJson(res, 200, result);
    })
}


// navedba modelov
module.exports.navediModele = function(req, res){
    //console.log(req);
    var data = req.body;
    
    Avtomobil.find({"znamka": req.params.znamka}).exec(function(err, result){
        returnJson(res, 200, result);
    })
} 
module.exports.uporabnikoviAvtomobili = function(req, res){
    
    
    //console.log('heyho');
    var username = req.body.uporabniskoIme;
    
    Avtomobil.find({'lastnik': username}).exec(function(err, result){
        //console.log(result);
        returnJson(res, 200, result);
    })
}