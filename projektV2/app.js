var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

require('./app_api/models/db');

var indexApi = require('./app_api/routes/index');
var index = require('./app_server/routes/index');
var faq = require('./app_server/routes/faq');
var avtomobili = require('./app_server/routes/avtomobili');
var dodajAvto = require('./app_server/routes/dodajAvto');
var dodajTocenjeGoriva = require('./app_server/routes/dodajTocenjeGoriva');
var editProfile = require('./app_server/routes/editProfile');
var indexSignedIn = require('./app_server/routes/indexSignedIn');
var kalkulator = require('./app_server/routes/kalkulator');
var kontakt = require('./app_server/routes/kontakt');
var signin = require('./app_server/routes/signin');
var signup = require('./app_server/routes/signup');
var db = require('./app_server/routes/db');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'app_server', 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/faq', faq);
app.use('/avtomobili', avtomobili);
app.use('/dodajavto', dodajAvto);
app.use('/dodajtocenje', dodajTocenjeGoriva);
app.use('/edit', editProfile);
app.use('/home', indexSignedIn);
app.use('/kalkulator', kalkulator);
app.use('/kontakt', kontakt);
app.use('/login', signin);
app.use('/register', signup);
app.use('/db', db);

app.use('/api', indexApi);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};


  // render the error page
  var error = err.status || 500
  res.status(error);
  res.write("Error happened: " + error);
  res.end();
});

module.exports = app;
