$(document).ready(function() {
    
    for (var i in myCars)
        $("#myCars").append(`<option value="${myCars[i].id}">${myCars[i].znamka} ${myCars[i].model}</option>`);
    
    $("#dodajButton").click(function(event) {
        $("#errorMessage").hide();
        $("#succesMessage").hide();
        
        var d = new Date($("#datum").val());
        
        var i = $("#myCars").val();
        
        var litri = $("#litri").val();
        
        var km = $("#kilometri").val();
        
        var opomba = $("#opombe").val();
        
        if (d.length < 1 || i.length < 1 || litri < 0.01 || km < 0.01) {
            $("#errorMessage").show();
        }
        else {
            save(d, i, litri, km, opomba);
            $("#succesMessage").show();
        }
        
        //console.log(myCars);
        
        event.preventDefault();
    });
});



function save(d, i, litri, km, opomba) {
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    
    for (var j in myCars) {
        if (myCars[j].id == i) {
            
            var done=false;
            var newDates = [];
            var newTankanja = [];
            var newStanja = [];
            var newOpombe = [];
            
            for (var k in myCars[j].dates) {
                
                if (!compare(myCars[j].dates[k].split("-"), day, month, year) && !done) {  //myCars[j].dates[k].split("-")  >= day-month-year
                    done=true;
                    newDates.push(day+"-"+month+"-"+year);
                    newTankanja.push(parseFloat(litri));
                    newStanja.push(parseFloat(km));
                    newOpombe.push(opomba);
                }
                
                newDates.push(myCars[j].dates[k]);
                newTankanja.push(myCars[j].tankanja[k]);
                newStanja.push(myCars[j].stanjeStevca[k]);
                newOpombe.push(myCars[j].opombe[k]);
            }
            
            if (done) {
                myCars[j].dates = newDates;
                myCars[j].tankanja = newTankanja;
                myCars[j].stanjeStevca = newStanja;
                myCars[j].opombe = newOpombe;
            } else {
                myCars[j].dates.push(day+"-"+month+"-"+year);
                myCars[j].tankanja.push(parseFloat(litri));
                myCars[j].stanjeStevca.push(parseFloat(km));
                myCars[j].opombe.push(opomba);
            }
        }
    }
}


var myCars = [
    {id: 1, znamka: "Honda", model: "Civic", tankanja: [45, 42, 48, 46] /* v Litrih */, stanjeStevca: [250367, 250717, 251117, 251517] /* v km */, 
        dates: ['15-9-2017', '3-10-2017', '28-10-2017', '25-11-2017'], opombe: ['', '', '', '']}
    ];
    
function compare(d1, day2, month2, year2) {
    var day1 = d1[0];
    var month1 = d1[1];
    var year1 = d1[2];
    
    if (year1 < year2) return true;
    else if (month1 < month2 && year1 == year2) return true;
    else if (day1 <= day2 && month1 == month2 && year1 == year2) return true;
    return false;
}