$(document).ready(function() {
    var znamka;
    $("#make").click(function(event) {
        znamka = $("#make").val();
	    
	    
	    if (znamka == "Audi") {
	        
	        //enable model selector
    	    $("#model").prop('disabled', false);
    	    
	        //add models to html
	        izprazniModel();
	        for (var i in audi) {
	            $("#model").append(`<option value="${audi[i].model}">${audi[i].model}</option>`);
	        }
	        
	    } else if (znamka == "Honda") {
	         //enable model selector
    	    $("#model").prop('disabled', false);
    	    
	        //add models to html
	        izprazniModel();
	        for (var i in honda) {
	            $("#model").append(`<option value="${honda[i].model}">${honda[i].model}</option>`);
	        }
	    }else {
	        
	        //disable model selector
	        $("#model").val("");
            $("#model").prop('disabled', true);
            
            //disable year selector
            $("#year").val("");
            $("#year").prop('disabled', true);
	    }
    });
    
    

    $("#model").click(function(event) {
        var model = $("#model").val();
        
        if (model.length > 0) {
            
            //enable year selector
            $("#year").prop('disabled', false);
            
        } else {
            //disable year selector
            $("#year").val("");
            $("#year").prop('disabled', true);
        }
    })
    
    
    $("#carSearchButton").click(function(event) {
        $("#resultContainer").empty();
        
        var znamka = $("#make").val();
        var model = $("#model").val();
        var letnik = $("#year").val();
        

        if (letnik.length > 0) {
            // izpisi vse z ustrezno znamko, modelom in letnikom
            for (var i in cars) {
                if (cars[i].znamka == znamka && cars[i].model == model && cars[i].letnik == letnik) {
                    izpisi(i);
                }
            }
            
        } else if (model.length > 0) {
            // izpisi vse z ustrezno znamko in modelom
            for (var i in cars) {
                if (cars[i].znamka == znamka && cars[i].model == model) {
                    izpisi(i);
                }
            }
            
        } else if (znamka.length > 0) {
            // izpisi vse z ustrezno znamko
            for (var i in cars) {
                if (cars[i].znamka == znamka) {
                    izpisi(i);
                }
            }
            
        } else {
            // izpisi vse
            for (var i in cars) {
                izpisi(i);
            }
        }
        event.preventDefault();
    });

	
	
	
});

var audi = [
	    {model: "50"},
	    {model: "60"},
	    {model: "80"},
	    {model: "90"},
	    {model: "100"},
	    {model: "200"},
        {model: "A1"},
        {model: "A2"},
        {model: "A3"},
        {model: "A4"},
        {model: "A5"},
        {model: "A6"},
        {model: "A7"},
        {model: "A8"},
        {model: "Allroad"},
        {model: "Coupe"},
        {model: "Cabriolet"},
        {model: "Q1"},
        {model: "Q2"},
        {model: "Q3"},
        {model: "Q5"},
        {model: "Q7"},
        {model: "R8"},
        {model: "RS2"},
        {model: "RS3"},
        {model: "RS4"},
        {model: "RS5"},
        {model: "RS6"},
        {model: "RS7"},
        {model: "RS Q3"},
        {model: "S1"},
        {model: "S2"},
        {model: "S3"},
        {model: "S4"},
        {model: "S5"},
        {model: "S6"},
        {model: "S7"},
        {model: "S8"},
        {model: "SQ5"},
        {model: "SQ7"},
        {model: "TT"},
        {model: "V8"}
	    ];
	    
var honda = [
    {model: "Civic"},
    {model: "Accord"}
    ];
	
	
var cars = [
    {uporabnik: "Marjan", znamka: "Audi", model: "A5", letnik: "2012", ostalo: "Sportback 2.0 TDI S-line"},
    {uporabnik: "jozek.honda", znamka: "Honda", model: "Civic", letnik: "1997", ostalo: "1.6 VTi VTEC"}
    ];
    
function izpisi(i) {
    $("#resultContainer").append(`<div>${cars[i].znamka}    ${cars[i].model}    ${cars[i].letnik}   ${cars[i].ostalo} &#8195;-&#8195; ${cars[i].uporabnik}</div>`);
}

function izprazniModel() {
    $("#model").empty();
	$("#model").append(`<option value="">Izberite letnik</option>`);
}