$(document).ready(function() {
    
	$("#loginbutton").click(function(event) {
		$("#errorMessage").hide();
	    $("#errorMessage1").hide();
	    $("#errorMessage2").hide();
		var username = $("#signup-username").val();
		var password = $("#signup-password").val();
		var passwordConfirm = $("#signup-password-confirm").val();
		
		if (username.length < 1 ||  $("#signup-email").val().length < 1 || password.length < 1 || passwordConfirm.length < 1) {
			$("#errorMessage").show();
		} else if (password != passwordConfirm) {
			$("#errorMessage2").show();
		} else if (username == "jozek.honda") {
		    $("#errorMessage1").show();
		} else {
			window.location = "index.html";
		}
		event.preventDefault();
	});

});