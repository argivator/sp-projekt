$(document).ready(function() {


    for (var i in cars) {
            $("#myCars").append(`<div class="rowCars"><div class="car">${cars[i].znamka} ${cars[i].model} ${cars[i].letnik} ${cars[i].ostalo}</div><div class="remove"><span class="glyphicon glyphicon-remove pejtLevo"></span></div></div>`);
    }
    
    for (var i in myCars) {
        for (var j in myCars[i].dates){
            $("#tocenja").append(`<div class="rowCars"><div class="car">${myCars[i].dates[j]} &#8195;-&#8195; ${myCars[i].znamka} ${myCars[i].model} ${myCars[i].letnik} ${myCars[i].ostalo}</div><div class="remove"><span class="glyphicon glyphicon-remove pejtLevo"></span></div></div>`);
        }
    }
    
    
    $("#passChangeButton").click(function() {
        $("#errorMessage").hide();
        $("#succesMessage").hide();
        $("#errorMessage1").hide();
        
        var p1 = $("#password").val();
        var p2 = $("#passwordConfirm").val();
        
        if (p1.length < 1 || p2.length < 1) {
            $("#errorMessage1").show();
        }
        else if (p1 == p2) {
            $("#succesMessage").show();
        } else {
            $("#errorMessage").show();
        }
        event.preventDefault();
    });

   
});


var cars = [
    {znamka: "Audi", model: "A5", letnik: "2012", ostalo: "Sportback 2.0 TDI S-line"},
    {znamka: "Honda", model: "Civic", letnik: "1997", ostalo: "1.6 VTi VTEC"},
    {znamka: "Honda", model: "Civic", letnik: "1997", ostalo: "1.4i S"}
    ];
    
var myCars = [
    {id: 1, znamka: "Honda", model: "Civic", letnik: "1997", ostalo: "1.6 VTi VTEC", tankanja: [45, 42, 48, 46] /* v Litrih */, stanjeStevca: [250367, 250717, 251117, 251517] /* v km */, 
        dates: ['15-9-2017', '3-10-2017', '28-10-2017', '25-11-2017'], opombe: ['', '', '', '']}
    ];