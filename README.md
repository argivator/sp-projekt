# TRETJI ZAGOVOR:
[Povezava do Heroku aplikacije](https://poraba.herokuapp.com/)

Zagon na c9.io:

	-zagon mongod (./mongod)
	
	-zagon npm v direktoriju projektV2 (npm start)
	
	-aplikacija dostopna na https://sp-projekt.{c9-username}.c9users.io
	
	Pripravljeni so 3-je uporabniki (PO UPORABI /ds):
	
	    - username: testing
	      password: testing
	      
	    - username: Mihec123
	      password: 123mihec
	      
	    - username: Asistent
	      password: asistent
	      
	Vsak od njih ima vnaprej podane avte v njegovi lasti.

# DRUGI ZAGOVOR:
Prva stran spletne aplikacije je: **index.html**
Za pravilno delovanje uporabniških funkcij je na voljo uporabnik: Username: **jozek.honda**  in koda:  **honda**.

Aplikacija je bila testirana na chrome in firefox: razlike so pri uporabi input type-ov, saj firefox in chrome prikazeta izbiro npr. datuma drugace.
Aplikacija je bila testirana tudi na telefonih, na manjsih ekranih se navigacijska vrstica skrije, do nje pa se pride z klikom na gumb, ki se pojavi v zgornjem desnem kotu. Vsi elementi se tudi ustrezno premaknejo, da ne
zaidejo iz ekrana.


# PORABA

## Uvod
Namen aplikacije "PORABA" je pomoč uporabnikom, pri računanju povprečne porabe njihovih avtomobilov, kot tudi vizualizacija porabe iz preteklosti (npr. avto ima večjo porabo kot pred enim letom,
nekaj je narobe).

Aplikacija omogoča vsakemu uporabniku, da vnese svoje vozilo in vnese podatke o točenju goriva ob vsakem točenju (lahko tudi vnese podatke za nazaj če jih ima). Ob teh podatkih se uporabniku
izrisuje graf in izpisuje povprečna poraba. Ko ima vnesenih dovolj podatkov, lahko uporabi kalkulator stroškov, ki izračuna pričakovane stroške na poti, glede na preteklo porabo.

## Zaslonske maske

**Zaslonska maska 1:** spodnja zaslonska maska prikazuje prvo stran, kot je prikazana gostom (dokler se uporabnik ne vpiše/registrira)
![Home guest](https://bytebucket.org/argivator/sp-projekt/raw/8c4c6803137cf20e4c40d67758b9eaf230a6c6c8/docs/Home---guest.png?token=62e7a3e6d52486d4247c4b44feba332d85e3ed6c)

**Zaslonske maska 2:** to okno se odpre ob kliku na zavihek "AVTOMOBILI". Sem lahko dostopajo registrirani uporabniki in gosti.
Ob izbiri znamke, modela in letnika avtomobila se odpre seznam teh avtomobilov z različnimi motorji. Ob kliku na enega izmed njih
se odpre stran z povprečno porabo takšnega motorja. Tam lahko iz seznama izbereš točno določen avtomobil (od nekega uporabnika), kjer
vidiš podatke tistega avtomobila in lahko napišeš komentar (če si vpisan).

![Izbira avta](https://bytebucket.org/argivator/sp-projekt/raw/8c4c6803137cf20e4c40d67758b9eaf230a6c6c8/docs/Izbira_avta.png?token=4c4d47a48b85bdaeac814caa71626639fc2316db)

**Zaslonska maska 3:** stran za registracijo uporabnikov.
![sign_up](https://bytebucket.org/argivator/sp-projekt/raw/8c4c6803137cf20e4c40d67758b9eaf230a6c6c8/docs/sign_up.png?token=b194e51e1ae83938e2a3ec62b9fa38ce026910ba)

**Zaslonska maska 4:** stran za prijavo že registriranih uporabnikov.
![sign_in](https://bytebucket.org/argivator/sp-projekt/raw/8c4c6803137cf20e4c40d67758b9eaf230a6c6c8/docs/sign_in.png?token=bee95635fc08c0d20d69f124769cf9bee9445731)

**Zaslonska maska 5:** spodnja zaslonska maska prikazuje prvo stran, kot je prikazana uporabniku (ko že ima vnešene podatke o avtu in točenjih goriva)
Levo zgoraj je prikazan izbran avto (če jih ima več) in povprečna poraba vseh vnosov do sedaj. 
Na sredini se bo izrisal graf porabe avtomobila (z možnostjo dodajanja grafa ostalih avtomobilov za primerjavo)
Desno nad grafom so gumbi za dodajanje točenja goriva in za dodajanje novega avtomobila.
![Home guest](https://bytebucket.org/argivator/sp-projekt/raw/8c4c6803137cf20e4c40d67758b9eaf230a6c6c8/docs/Home-uporabnik.png?token=477f3d4de93e17506d8a16a2bcafc1366794a715)

**Zaslonska maska 6:** stran za dodajanje avtomobila. Tukaj uporabnik doda svoj avtomobil.
Najprej izbere znamko. Ob izbiri znamke se odpre izbira modela. Nato izbere letnik in na podlagi do sedaj izbranih podatkov dobi opcije motorjev.
Če njegovega avta (znamke, modela, letnika ali motorja) ni na seznamu, jo ima možnost dodati. V dodaten opis pa lahko napiše karkoli mu paše.
![dodaj_avto](https://bytebucket.org/argivator/sp-projekt/raw/8c4c6803137cf20e4c40d67758b9eaf230a6c6c8/docs/dodaj_avto.png?token=9a762b13125f4859efbbe06a7d50e3d2fe9b806c)

**Zaslonske maska 7:** stran za dodajanje točenja goriva. Uporabnik izbere datum točenja (lahko tudi iz preteklosti če ima podatke), nato izbere enega izmed svojih avtomobilov, koliko je natočil, ter stanje na števcu km(manjka na sliki).
Pod opombe si lahko zase zapiše naprimer kje je tankal, ceno, karkoli...
![dodaj_tocenje](https://bytebucket.org/argivator/sp-projekt/raw/8c4c6803137cf20e4c40d67758b9eaf230a6c6c8/docs/Dodaj_tocenje_goriva.png?token=287c87e24d93a48521e331fa88caf848d5758cf9)

**Zaslonska maska 8:** stran za urejanje profila. Tu lahko uporabnik vidi seznam svojih avtomobilov in seznam svojih točenj goriva.
Ob kliku na avtomobil oz. točenje lahko ureja vnos. Ob kliku na X desno pa lahko vnos izbriše. Na desni strani pa lahko zamenja svoje geslo.
![uredi profil](https://bytebucket.org/argivator/sp-projekt/raw/8c4c6803137cf20e4c40d67758b9eaf230a6c6c8/docs/uredi_profil.png?token=d10aab8e8ded6fc403dc0dc9f3b32c6b330345de)

**Zaslonske maska 9:** Ob kliku na "zobnik" na desni strani glave strani se odpre meni, kjer lahko uporabnik hitreje pride do dodajanja avtomobila
in dodajanja točenja goriva. Tukaj lahko dostopa do kalkulatorja stroškov (naslednja maska), urejanja profila ter se odjavi.
![meni](https://bytebucket.org/argivator/sp-projekt/raw/8c4c6803137cf20e4c40d67758b9eaf230a6c6c8/docs/meni.png?token=18e01c41e47bb2a45f78de943b1e999b484859cb)

**Zaslonske maska 10:** kalkulator stroškov. Tu lahko uporabnik izbere enega izmed svojih avtomobilov in vpiše dolžino poti na katero se podaja. Aplikacija
mu izračuna pričakovane stroške te poti glede na predhodne vnose točenj goriva, ter na trenutno ceno goriva.
![kalkulator](https://bytebucket.org/argivator/sp-projekt/raw/8c4c6803137cf20e4c40d67758b9eaf230a6c6c8/docs/kalkulator_stroskov.png?token=99aefa06d230e2455c6400559d5eb07987c30c5e)

## Mobilne zaslonske maske

**Zaslonska maska 1:** prva stran (prikaz za goste).
![mHome](https://bytebucket.org/argivator/sp-projekt/raw/338046d863258b4e46d567dfd0c6b86c2f2e0a7b/docs/Mobile_home_guest.png?token=8d472e6b116593d0e1ae5b4132de454ef51f5bc9)

**Zaslonske maska 2:** avtomobili.          
![mCar](https://bytebucket.org/argivator/sp-projekt/raw/338046d863258b4e46d567dfd0c6b86c2f2e0a7b/docs/Mobile_izbira_avta.png?token=775ec0438babb32215fc7546aed27efc469b6fae)

**Zaslonska maska 3:** stran za registracijo uporabnikov.
![mSignUp](https://bytebucket.org/argivator/sp-projekt/raw/338046d863258b4e46d567dfd0c6b86c2f2e0a7b/docs/Mobile_sign_up.png?token=bf93b629d4e21f66be59d5db5b3b91117aa12031)

**Zaslonska maska 4:** stran za prijavo že registriranih uporabnikov.
![mLogin](https://bytebucket.org/argivator/sp-projekt/raw/338046d863258b4e46d567dfd0c6b86c2f2e0a7b/docs/Mobile_sign_in.png?token=17ad577dfc0d60940c45713c62fc01f4c820c9a5)

**Zaslonska maska 5:** prva stran (prikaz za vpisane uporabnike)
![mHomeUser](https://bytebucket.org/argivator/sp-projekt/raw/338046d863258b4e46d567dfd0c6b86c2f2e0a7b/docs/Mobile_home_uporabnik.png?token=0c479799f008faa0bbc75dcccd76dc4d641a5792)

**Zaslonska maska 6:** stran za dodajanje avtomobila.
![mAddCar](https://bytebucket.org/argivator/sp-projekt/raw/338046d863258b4e46d567dfd0c6b86c2f2e0a7b/docs/Mobile_dodaj_avto.png?token=8051565b2e5c6dcb20df47fd3e97c3586c174d0c)

**Zaslonske maska 7:** stran za dodajanje točenja goriva. 
![mFillUp](https://bytebucket.org/argivator/sp-projekt/raw/338046d863258b4e46d567dfd0c6b86c2f2e0a7b/docs/Mobile_dodaj_tocenje.png?token=2fec965d6c4bdae7c02dcb38633ffdfa231b6d25)
**Zaslonska maska 8:** stran za urejanje profila.      
![mEditProfile](https://bytebucket.org/argivator/sp-projekt/raw/338046d863258b4e46d567dfd0c6b86c2f2e0a7b/docs/Mobile_uredi_profil.png?token=7601bc8a2882d88eaa7c02a9782c95954434900a)

**Zaslonske maska 9:** mobilni meni.          
![mMenu](https://bytebucket.org/argivator/sp-projekt/raw/338046d863258b4e46d567dfd0c6b86c2f2e0a7b/docs/Mobile_meni.png?token=92e9dcff578b0fb27a8c6e4f9ca1f92bb408b878)

**Zaslonske maska 10:** kalkulator stroškov.        
![mKalkulator](https://bytebucket.org/argivator/sp-projekt/raw/338046d863258b4e46d567dfd0c6b86c2f2e0a7b/docs/Mobile_kalkulator.png?token=0536a078475abc1a388f7be469a3b6ae2376672b)